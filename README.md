# Image Uploader App
This is a REST api allowing user to upload image and generate thumbnails according to user's subscription plan. 
Only Admin user is able to create users and associated arbitrary subscription plans
 

## Setup

Note that project uses .env file to store sensitive data. For the project launch, either create one with SECRET_KEY or update it directly in settings.py

Install Python requirements
```bash
pip install -r requirements.txt
```
Initialize database (SQLite)
```bash
python manage.py makemigrations
python manage.py migrate
```
Create superuser/admin
```bash
python manage.py createsuperuser
```

Finally, run local webserver:
```bash
python manage.py runserver
```

Open app in a browser at: http://127.0.0.1:8000/

To login to admin UI go to: http://127.0.0.1:8000/admin/

Other endpoints:
* http://127.0.0.1:8000/image/upload/
* http://127.0.0.1:8000/image/list/
* http://127.0.0.1:8000/image/get/<int>/