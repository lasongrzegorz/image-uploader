from django.contrib.auth.models import User
from django.db import models
from django.core.validators import MinValueValidator, MaxValueValidator


class Image(models.Model):
    image = models.ImageField(upload_to="images", null=True, blank=True)

    def __str__(self):
        return self.image.name


class AccountTier(models.Model):
    name = models.CharField(max_length=120)
    expiration_time = models.IntegerField(
        null=True,
        blank=True,
        validators=[
            MinValueValidator(300, "Expiration time cannot be lower than 300"),
            MaxValueValidator(30000, "Expiration time cannot be larger than 30000"),
        ],
    )
    original_image_link_available = models.BooleanField(
        verbose_name="Grant access to original photo?"
    )

    def __str__(self):
        return self.name


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    account_tier = models.ForeignKey(
        "AccountTier", on_delete=models.CASCADE, null=True, blank=True
    )

    def __str__(self):
        return self.user.username


class ThumbnailHeight(models.Model):
    height = models.IntegerField(unique=True)
    account_tier = models.ForeignKey("AccountTier", on_delete=models.CASCADE)

    def __str__(self):
        return f"{self.account_tier} - {self.height}"
