from django.urls import path

from image_uploader import views

urlpatterns = [
    path("upload/", views.CreateImageView.as_view(), name="image-upload"),
    path("list/", views.ListImageView.as_view(), name="image-list"),
    path("get/<int:pk>/", views.GetThumbnailView.as_view(), name="image-get"),
]
