from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.models import User
from image_uploader.models import Image, AccountTier, Profile, ThumbnailHeight


class ThumbnailHeightInline(admin.StackedInline):
    model = ThumbnailHeight
    extra = 1


class AccountTierAdmin(admin.ModelAdmin):
    inlines = [ThumbnailHeightInline]


class ProfileInline(admin.StackedInline):
    model = Profile
    can_delete = False


class UserAdmin(BaseUserAdmin):
    inlines = (ProfileInline,)


admin.site.register(Image)
admin.site.register(AccountTier, AccountTierAdmin)
admin.site.unregister(User)
admin.site.register(User, UserAdmin)
