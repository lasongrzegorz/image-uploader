from rest_framework.response import Response
from rest_framework import status

from image_uploader.models import Image, AccountTier
from rest_framework.views import APIView
from rest_framework.generics import CreateAPIView, ListAPIView
from image_uploader import serializers
from easy_thumbnails.files import get_thumbnailer


class CreateImageView(CreateAPIView):
    serializer_class = serializers.ImageSerializer
    queryset = Image.objects.all()


class ListImageView(ListAPIView):
    serializer_class = serializers.ImageSerializer
    queryset = Image.objects.all()


class GetThumbnailView(APIView):
    def get(self, request, pk):

        orig_image = Image.objects.get(id=pk)
        try:
            account_tier = AccountTier.objects.get(profile__user=request.user)
        except AccountTier.DoesNotExist:
            return Response(
                {"image": ["No account tier assigned to user"]}, status=status.HTTP_400_BAD_REQUEST
            )

        heights = [obj.height for obj in account_tier.thumbnailheight_set.all()]
        thumbnails = {}
        for height in heights:
            options = {"size": (0, height), "crop": True}
            thumbnails[height] = (
                get_thumbnailer(orig_image.image).get_thumbnail(options).url
            )

        data = [thumbnails]

        if account_tier.original_image_link_available:
            original_image_url = serializers.ImageSerializer(orig_image).data
            data.append(original_image_url)

        return Response(data)
